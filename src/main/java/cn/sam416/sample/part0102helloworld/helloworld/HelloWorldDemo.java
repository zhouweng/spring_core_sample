package cn.sam416.sample.part0102helloworld.helloworld;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloWorldDemo {

        public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0102helloworld/hello_bean.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        HelloWorld helloWorld = context.getBean("helloWorld", HelloWorld.class);
        helloWorld.say();

    }
}
