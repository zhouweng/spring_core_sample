package cn.sam416.sample.part0109annotation.autowired;

import org.springframework.beans.factory.annotation.Autowired;

public class MovieRecommender {
    private final CustomerPreferenceDao customerPreferenceDao;

    // @1 this @Autowired can be removed, because there's only one constructor
    @Autowired
    public MovieRecommender(CustomerPreferenceDao customerPreferenceDao) {
        this.customerPreferenceDao = customerPreferenceDao;
    }
}
