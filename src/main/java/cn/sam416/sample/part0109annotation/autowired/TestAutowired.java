package cn.sam416.sample.part0109annotation.autowired;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestAutowired {

        public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0109annotation/autowired-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        MovieRecommender movieRecommender = context.getBean("movieRecommender", MovieRecommender.class);
        System.out.println(movieRecommender);

    }
}
