package cn.sam416.sample.part0109annotation.primary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//From: https://blog.csdn.net/CNZYYH/article/details/88529501
//remark @Primary will NoUniqueBeanDefinitionException: No qualifying bean of type 'cn.sam416.sample.part0109annotation.primary.Singer'
@Primary
@Component("metalSinger")
class MetalSinger implements Singer{
        @Override
        public String sing(String lyrics) {
                return "I am singing with DIO voice: "+lyrics;
        }
}


@Component("operaSinger")
class OperaSinger implements Singer {
        @Override
        public String sing(String lyrics) {
                return "I am singing in Bocelli voice: "+lyrics;
        }
}

interface Singer {
        String sing(String lyrics);
}

@Component
public class SingerService {
        @Autowired
//@1 open Qualifier for other Singer
//        @Qualifier("operaSinger")
        private Singer singer;

        public String sing() {
                return singer.sing("song lyrics");
        }

        public static void main(String[] args) {
                ApplicationContext context = new AnnotationConfigApplicationContext("cn.sam416.sample.part0109annotation.primary");
                SingerService singerService = context.getBean(SingerService.class);
                System.out.println(singerService.sing());
        }
}