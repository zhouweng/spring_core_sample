package cn.sam416.sample.part0101basic.servicelocator;

import java.util.Date;

public class ServiceLocator {
    private static Cache cache;

    static {
        cache = new Cache();
    }

    public static Service getService(String jndiName){
        long before = new Date().getTime();
        Service service = cache.getService(jndiName);

        if(service != null){
            long after = new Date().getTime();
            System.out.println("time cost:"+(after-before));
            return service;
        }

        InitialContext context = new InitialContext();
        Service serviceNotIn = (Service)context.lookup(jndiName);
        cache.addService(serviceNotIn);
        long after = new Date().getTime();
        System.out.println("time cost:"+(after-before));
        return serviceNotIn;
    }
}