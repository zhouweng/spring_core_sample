package cn.sam416.sample.part0101basic.servicelocator;

public interface Service {
    public String getName();
    public void execute();
}