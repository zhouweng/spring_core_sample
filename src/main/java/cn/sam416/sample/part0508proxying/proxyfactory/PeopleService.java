package cn.sam416.sample.part0508proxying.proxyfactory;

public interface PeopleService {
    public void sayHello();

    public void printName(String name);
}