package cn.sam416.sample.part0508proxying.proxyfactory;

import org.springframework.aop.framework.ProxyFactory;

//From: https://blog.csdn.net/sunnycoco05/article/details/78901449
public class ProxyFactoryTest {

    public static void main(String[] args) {
        //代理对象未指定接口，使用CGLIB生成代理类
        ProxyFactory factory1 = new ProxyFactory();
        factory1.setTarget(new MyTarget());
        factory1.addAdvice(new AroundInteceptor());
        MyTarget targetProxy = (MyTarget) factory1.getProxy();
        targetProxy.printName();
        System.err.println(targetProxy.getClass().getName());

        //代理对象指定接口PeopleService，目标类为实现PeopleService的EnglishService，使用JDK proxy生成代理类
        ProxyFactory factory2 = new ProxyFactory();
        factory2.setInterfaces(new Class[] { PeopleService.class });
        factory2.addAdvice(new AroundInteceptor());
        factory2.setTarget(new EnglishService());
        PeopleService peopleProxy = (PeopleService) factory2.getProxy();
        peopleProxy.sayHello();
        System.err.println(peopleProxy.getClass().getName());
    }
}
