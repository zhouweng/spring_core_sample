package cn.sam416.sample.part0107inheritance.beandefinitionregistry;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;

import java.util.Arrays;

// From: https://blog.csdn.net/likun557/article/details/105355394?utm_medium=distribute.pc_feed.none-task-blog-alirecmd-24&depth_1-utm_source=distribute.pc_feed.none-task-blog-alirecmd-24&request_id=
public class BeanDefinitionRegistryTest {
        public static void main(String[] args) {
                //创建一个bean工厂，这个默认实现了BeanDefinitionRegistry接口，所以也是一个bean注册器
                DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

                //定义一个bean
                GenericBeanDefinition nameBdf = new GenericBeanDefinition();
                nameBdf.setBeanClass(String.class);
                nameBdf.getConstructorArgumentValues().addIndexedArgumentValue(0, "路人甲Java");

                //将bean注册到容器中
                factory.registerBeanDefinition("name", nameBdf);

                //通过名称获取BeanDefinition
                System.out.println("1:"+factory.getBeanDefinition("name"));
                //通过名称判断是否注册过BeanDefinition
                System.out.println("2:"+factory.containsBeanDefinition("name"));
                //获取所有注册的名称
                System.out.println("3:"+Arrays.asList(factory.getBeanDefinitionNames()));
                //获取已注册的BeanDefinition的数量
                System.out.println("4:"+factory.getBeanDefinitionCount());
                //判断指定的name是否使用过
                System.out.println("5:"+factory.isBeanNameInUse("name"));

                //别名相关方法
                //为name注册2个别名
                factory.registerAlias("name", "alias-name-1");
                factory.registerAlias("name", "alias-name-2");

                //判断alias-name-1是否已被作为别名使用
                System.out.println("6:"+factory.isAlias("alias-name-1"));

                //通过名称获取对应的所有别名
                System.out.println("7:"+Arrays.asList(factory.getAliases("name")));

                //最后我们再来获取一下这个bean
                System.out.println("8:"+factory.getBean("name"));

        }

}