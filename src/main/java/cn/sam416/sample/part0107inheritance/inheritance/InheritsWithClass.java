package cn.sam416.sample.part0107inheritance.inheritance;

public class InheritsWithClass {
    String name;
    int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    void initialize() {
        System.out.println("InheritsWithClass.initialize()");
    }
    @Override
    public String toString() {
        return "InheritsWithClass{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
