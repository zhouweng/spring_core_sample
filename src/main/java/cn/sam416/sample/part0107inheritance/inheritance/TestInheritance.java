package cn.sam416.sample.part0107inheritance.inheritance;

import cn.sam416.sample.part0102helloworld.helloworld.HelloWorld;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestInheritance {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0107inheritance/inheritance-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        InheritsWithClass inheritsWithClass = context.getBean("inheritsWithClass", InheritsWithClass.class);
        System.out.println(inheritsWithClass);

        //@1 BeanIsAbstractException
        // InheritedTestBeanWithoutClass InheritedTestBeanWithoutClass = context.getBean("inheritedTestBeanWithoutClass", InheritedTestBeanWithoutClass.class);

    }
}
