package cn.sam416.sample.part0103instantiatingbeans.staticfactory;

public class StaticFactoryService {
    private static StaticFactoryService clientService = new StaticFactoryService();
    private StaticFactoryService() {
        System.out.println("StaticFactoryService()");

    }

    public static StaticFactoryService createInstance() {
        System.out.println("StaticFactoryService.createInstance()()");
        return clientService;
    }
}
