package cn.sam416.sample.part0103instantiatingbeans.staticfactory;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestStaticFactory {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0103instantiatingbeans/beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        StaticFactoryService clientService = context.getBean("staticFactoryService", StaticFactoryService.class);

    }
}
