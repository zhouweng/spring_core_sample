package cn.sam416.sample.part0103instantiatingbeans.instancefactory;


import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestInstanceFactory {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0103instantiatingbeans/beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        InstanceFactoryService instanceFactoryService = context.getBean("instanceFactoryService", InstanceFactoryService.class);

    }
}
