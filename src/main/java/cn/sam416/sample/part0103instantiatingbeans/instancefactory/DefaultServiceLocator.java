package cn.sam416.sample.part0103instantiatingbeans.instancefactory;

public class DefaultServiceLocator {

    private static InstanceFactoryService clientService = new InstanceFactoryServiceImpl();

    public InstanceFactoryService createClientServiceInstance() {
        System.out.println("InstanceFactoryService.createClientServiceInstance()");
        return clientService;
    }
}