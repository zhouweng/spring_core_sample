package cn.sam416.sample.part0103instantiatingbeans.constructor;


import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestConstructorBean {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0103instantiatingbeans/beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        ConstructorBean constructorBean = context.getBean("constructorBean", ConstructorBean.class);

    }
}
