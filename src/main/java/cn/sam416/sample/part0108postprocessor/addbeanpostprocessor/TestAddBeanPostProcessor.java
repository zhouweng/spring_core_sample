package cn.sam416.sample.part0108postprocessor.addbeanpostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;

public class TestAddBeanPostProcessor {
    public static void main(String[] args) {
           DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
 
        factory.registerBeanDefinition("user1", BeanDefinitionBuilder.
                                genericBeanDefinition(UserModel.class).
                                addPropertyValue("name", "路人甲Java").
                                addPropertyValue("age", 30).
                                getBeanDefinition());
 
        factory.registerBeanDefinition("user2", BeanDefinitionBuilder.
                                genericBeanDefinition(UserModel.class).
                                addPropertyValue("name", "刘德华").
                                addPropertyValue("age", 50).
                                getBeanDefinition());

        factory.addBeanPostProcessor(new InstantiationAwareBeanPostProcessor() {
            @Override
            public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
                if ("user1".equals(beanName)) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        
        for (String beanName : factory.getBeanDefinitionNames()) {
            System.out.println(String.format("%s->%s", beanName, factory.getBean(beanName)));
        }

        
    }
}
