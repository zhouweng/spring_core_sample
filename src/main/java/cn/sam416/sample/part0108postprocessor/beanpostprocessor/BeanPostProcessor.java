package cn.sam416.sample.part0108postprocessor.beanpostprocessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanPostProcessor {
        public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0108postprocessor/postprocessor-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        Messenger messenger = context.getBean("messenger", Messenger.class);
        System.out.println(messenger);
    }
}
