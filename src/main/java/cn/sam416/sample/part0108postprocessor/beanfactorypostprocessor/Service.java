package cn.sam416.sample.part0108postprocessor.beanfactorypostprocessor;


public class Service {

    private String desc = "desc from class";

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getServiceDesc() {
        return desc;
    }
}