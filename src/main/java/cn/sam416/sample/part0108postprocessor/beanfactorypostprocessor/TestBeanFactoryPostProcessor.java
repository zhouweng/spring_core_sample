package cn.sam416.sample.part0108postprocessor.beanfactorypostprocessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://blog.csdn.net/boling_cavalry/article/details/82083889
public class TestBeanFactoryPostProcessor {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0108postprocessor/beanfactorypostprocessor-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        Service service = context.getBean("service", Service.class);
        System.out.println(service.getServiceDesc());

    }

}
