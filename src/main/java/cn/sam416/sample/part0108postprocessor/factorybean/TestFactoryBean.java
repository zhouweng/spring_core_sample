package cn.sam416.sample.part0108postprocessor.factorybean;

import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://www.cnblogs.com/tiancai/p/9604040.html
public class TestFactoryBean {
    public static void main(String[] args){
        String url = "classpath:/cn/sam416/sample/part0108postprocessor/factorybean-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(url);
        Student student =  context.getBean("student",Student.class);
        Object school=  context.getBean("factoryBeanPojo");
        FactoryBeanPojo factoryBeanPojo= (FactoryBeanPojo) context.getBean("&factoryBeanPojo");
        System.out.println(school);
        System.out.println(student);
        System.out.println(school.getClass().getName());
        System.out.println(factoryBeanPojo.getClass().getName());
    }

}
