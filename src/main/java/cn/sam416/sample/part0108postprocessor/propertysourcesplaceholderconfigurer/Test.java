package cn.sam416.sample.part0108postprocessor.propertysourcesplaceholderconfigurer;

import cn.sam416.sample.part0102helloworld.helloworld.HelloWorld;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

        public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0108postprocessor/propertysourcesplaceholderconfigurer-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        DriverManagerDataSource driverManagerDataSource = context.getBean("driverManagerDataSource", DriverManagerDataSource.class);
        System.out.println(driverManagerDataSource);

    }
}
