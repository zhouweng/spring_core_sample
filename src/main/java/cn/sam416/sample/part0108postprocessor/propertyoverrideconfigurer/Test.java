package cn.sam416.sample.part0108postprocessor.propertyoverrideconfigurer;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://blog.csdn.net/daryl715/article/details/1546343
public class Test {
        public static void main(String[] args) {
                String beanXml = "classpath:/cn/sam416/sample/part0108postprocessor/propertyoverrideconfigurer-beans.xml";
                ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
                Chinese chinese = context.getBean("chinese", Chinese.class);
                System.out.println(chinese);
//                FactoryBean factoryBean = context.getBean("&chinese", FactoryBean.class);
//                System.out.println(factoryBean);
        }
}
