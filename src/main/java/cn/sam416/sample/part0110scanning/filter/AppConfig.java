package cn.sam416.sample.part0110scanning.filter;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Service;

@Configuration
@ComponentScan(basePackages = "cn.sam416.sample.part0110scanning.filter"
        ,excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "cn.sam416.sample.part0110scanning.filter.*B")
        ,includeFilters = @ComponentScan.Filter(Service.class)
)
public class AppConfig {
}
