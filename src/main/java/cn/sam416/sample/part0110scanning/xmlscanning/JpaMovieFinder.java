package cn.sam416.sample.part0110scanning.xmlscanning;

import cn.sam416.sample.part0110scanning.componentscan.MovieFinder;
import org.springframework.stereotype.Repository;

@Repository
public class JpaMovieFinder implements MovieFinder {
    // implementation elided for clarity
}