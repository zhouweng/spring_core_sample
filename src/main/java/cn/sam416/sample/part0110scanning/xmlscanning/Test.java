package cn.sam416.sample.part0110scanning.xmlscanning;

import cn.sam416.sample.part0102helloworld.helloworld.HelloWorld;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0110scanning/component-scan-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
            String[] beanDefinitionNames = context.getBeanDefinitionNames();
            for (String beanName : beanDefinitionNames) {
                    System.out.println("beanName: " + beanName);
            }
    }
}
