package cn.sam416.sample.part0110scanning.xmlscanning;

import cn.sam416.sample.part0110scanning.componentscan.MovieFinder;
import org.springframework.stereotype.Service;

@Service
public class SimpleMovieLister {

    private MovieFinder movieFinder;

    public SimpleMovieLister(MovieFinder movieFinder) {
        this.movieFinder = movieFinder;
    }
}