package cn.sam416.sample.part0110scanning.componentbean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
        public static void main(String[] args) {
                ApplicationContext applicationContext =
                        new AnnotationConfigApplicationContext(AppConfig.class);

                String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
                for (String beanName : beanDefinitionNames) {
                        System.out.println("beanName: " + beanName);
                }

                printTestBean(applicationContext, "publicInstance");
                printTestBean(applicationContext, "protectedInstance");
                printTestBean(applicationContext, "privateInstance");
        }

        static void printTestBean(ApplicationContext applicationContext, String testBean) {
                TestBean instance = applicationContext.getBean(testBean,TestBean.class);
                System.out.println(instance);
                System.out.println(instance.say());

        }
}
