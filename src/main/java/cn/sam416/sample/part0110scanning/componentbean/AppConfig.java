package cn.sam416.sample.part0110scanning.componentbean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "cn.sam416.sample.part0110scanning.componentbean")
public class AppConfig {
}
