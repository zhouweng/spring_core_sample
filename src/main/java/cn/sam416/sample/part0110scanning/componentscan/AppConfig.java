package cn.sam416.sample.part0110scanning.componentscan;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "cn.sam416.sample.part0110scanning.componentscan")
public class AppConfig {
}
