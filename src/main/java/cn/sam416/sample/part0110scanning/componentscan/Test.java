package cn.sam416.sample.part0110scanning.componentscan;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
        public static void main(String[] args) {
                ApplicationContext applicationContext =
                        new AnnotationConfigApplicationContext(AppConfig.class);

                String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
                for (String beanName : beanDefinitionNames) {
                        System.out.println("beanName: " + beanName);
                }
        }
}
