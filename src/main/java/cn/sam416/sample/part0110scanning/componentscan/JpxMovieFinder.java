package cn.sam416.sample.part0110scanning.componentscan;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class JpxMovieFinder implements MovieFinder {
    // implementation elided for clarity
}