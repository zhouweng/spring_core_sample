package cn.sam416.sample.part0110scanning.configurationbean;

public class TestBean {
    String name;
    public int age; //@1 Why this must be public?
    String country;
    TestBean spouse;

    public TestBean(String name) {
        this.name = name;
    }
    public TestBean(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {this.age = age;    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSpouse(TestBean spouse) {
        this.spouse = spouse;
    }

    public String say() {
        return "TestBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", country='" + country + '\'' +
                ", spouse=" + spouse +
                '}';
    }
}
