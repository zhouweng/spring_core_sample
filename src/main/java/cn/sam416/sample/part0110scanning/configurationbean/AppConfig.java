package cn.sam416.sample.part0110scanning.configurationbean;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Indexed;

@Configuration
//@ComponentScan(basePackages = "cn.sam416.sample.part0110scanning.configurationbean")
public class AppConfig {    private static int i=10;

    @Bean
    @Qualifier("public")
    public TestBean publicInstance() {
        return new TestBean("publicInstance");
    }

    // use of a custom qualifier and autowiring of method parameters
    @Bean
    protected TestBean protectedInstance(
            @Qualifier("public") TestBean spouse
//            , @Value("#{privateInstance.age}") String country
    ) {
        TestBean tb = new TestBean("protectedInstance", 1);
        tb.setSpouse(spouse);
//        tb.setCountry(country);
        return tb;
    }
// BeanDefinitionParsingException: Configuration problem: @Bean method 'privateInstance' must not be private or final;
//    @Bean
//    private TestBean privateInstance() {
//        return new TestBean("privateInstance", ++i);
//    }

}
