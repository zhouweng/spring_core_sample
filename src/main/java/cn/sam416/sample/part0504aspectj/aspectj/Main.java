package cn.sam416.sample.part0504aspectj.aspectj;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://www.cnblogs.com/ooo0/p/11018740.html
public class Main {
    public static void main(String[] args) {
        boolean isXML = true;
        ApplicationContext ctx=null;
        if(isXML) {
// @2 XML
            ctx = new ClassPathXmlApplicationContext("classpath:/cn/sam416/sample/part0504aspectj/aspectj-beans.xml");
        } else {
// @1 annotation
            ctx = new AnnotationConfigApplicationContext(AopConfig.class);
        }
        RoleService roleService = ctx.getBean(RoleService.class);
        roleService.printRole("-helloWorld-"+isXML);
        System.out.println("####################");

    }

}
