package cn.sam416.sample.part0504aspectj.aspectj;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
public class RoleAspect {

    @Pointcut("execution(* cn.sam416.sample.part0504aspectj.aspectj.RoleServiceImpl.printRole(..))")
    public void print() {
    }

    @Before("print()")
    public void before() {
        System.out.println("before ....");
    }

    @After("execution(* cn.sam416.sample.part0504aspectj.aspectj.RoleServiceImpl.printRole(..))")
    public void after() {
        System.out.println("after ....");
    }

    @AfterReturning("execution(* cn.sam416.sample.part0504aspectj.aspectj.RoleServiceImpl.printRole(..))")
    public void afterReturning() {
        System.out.println("afterReturning ....");
    }

    @AfterThrowing("execution(* cn.sam416.sample.part0504aspectj.aspectj.RoleServiceImpl.printRole(..))")
    public void afterThrowing() {
        System.out.println("afterThrowing ....");
    }

    @Around("print()")
    public void around(ProceedingJoinPoint jp) {
        System.out.println("around before ....");
        try {
            jp.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.out.println("around after ....");
    }

}