package cn.sam416.sample.part0504aspectj.aspectj;

import org.springframework.stereotype.Component;

@Component
public class RoleServiceImpl implements RoleService {
    public void printRole(String message) {
        System.out.println("RoleServiceImpl.printRole()"+message);
    }

}