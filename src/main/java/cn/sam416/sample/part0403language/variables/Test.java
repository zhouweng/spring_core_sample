package cn.sam416.sample.part0403language.variables;

import cn.sam416.sample.part0403language.Inventor;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//From: 4.3.10. Variables
public class Test {
    public static void main(String[] args) {
        {
            ExpressionParser parser = new SpelExpressionParser();

            Inventor tesla = new Inventor("Nikola Tesla", "Serbian");

            EvaluationContext context = SimpleEvaluationContext.forReadWriteDataBinding().build();
            context.setVariable("newName", "Mike Tesla");

            parser.parseExpression("Name = #newName").getValue(context, tesla);
            System.out.println(tesla.getName());  // "Mike Tesla"
        }
        {
            // create an array of integers
            List<Integer> primes = new ArrayList<Integer>();
            primes.addAll(Arrays.asList(2, 3, 5, 7, 11, 13, 17));
// create parser and set variable 'primes' as the array of integers
            ExpressionParser parser = new SpelExpressionParser();
            EvaluationContext context = SimpleEvaluationContext.forReadWriteDataBinding().build();
            context.setVariable("primes", primes);
// all prime numbers > 10 from the list (using selection ?{...})
// evaluates to [11, 13, 17]
            List<Integer> primesGreaterThanTen = (List<Integer>) parser.parseExpression(
                    "#primes.?[#this>10]").getValue(context);
            System.out.println(primesGreaterThanTen);
        }
    }
}
