package cn.sam416.sample.part0403language.assignmentoperator;

import cn.sam416.sample.part0403language.Inventor;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

// 4.3.7. Operators - The Assignment Operator
public class Test {
    public static void main(String[] args) {

        ExpressionParser parser = new SpelExpressionParser();

        Inventor inventor = new Inventor();
        EvaluationContext context = SimpleEvaluationContext.forReadWriteDataBinding().build();

        parser.parseExpression("Name").setValue(context, inventor, "Aleksandar Seovic");

// alternatively
        String aleks = parser.parseExpression(
                "Name = 'Aleksandar Seovic'").getValue(context, inventor, String.class);
        System.out.println(aleks); // Aleksandar Seovic

    }
}
