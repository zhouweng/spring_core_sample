package cn.sam416.sample.part0403language.logicaloperators;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

// 4.3.7. Operators - Logical Operators
public class Test {
    public static void main(String[] args) {

        ExpressionParser parser = new SpelExpressionParser();
        // -- AND --

// evaluates to false
        boolean falseValue = parser.parseExpression("true and false").getValue(Boolean.class);
        System.out.println(falseValue);

// evaluates to true
        String expression = "isMember('Nikola Tesla') and isMember('Mihajlo Pupin')";
//        boolean trueValue = parser.parseExpression(expression).getValue(societyContext, Boolean.class);

// -- OR --

// evaluates to true
        boolean trueValue = parser.parseExpression("true or false").getValue(Boolean.class);
        System.out.println(trueValue);
// evaluates to true
        expression = "isMember('Nikola Tesla') or isMember('Albert Einstein')";
//        boolean trueValue = parser.parseExpression(expression).getValue(societyContext, Boolean.class);

// -- NOT --

// evaluates to false
        falseValue = parser.parseExpression("!true").getValue(Boolean.class);
        System.out.println(falseValue);


// -- AND and NOT --
        expression = "isMember('Nikola Tesla') and !isMember('Mihajlo Pupin')";
//        boolean falseValue = parser.parseExpression(expression).getValue(societyContext, Boolean.class);
//
    }
}
