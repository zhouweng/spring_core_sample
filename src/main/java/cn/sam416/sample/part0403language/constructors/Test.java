package cn.sam416.sample.part0403language.constructors;

import cn.sam416.sample.part0403language.Inventor;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

//From: 4.3.9. Constructors
public class Test {
    public static void main(String[] args) {
        ExpressionParser parser = new SpelExpressionParser();
//        EvaluationContext societyContext = SimpleEvaluationContext.forReadOnlyDataBinding().build();
//
        Inventor einstein = parser.parseExpression(
                "new cn.sam416.sample.part0403language.Inventor('Albert Einstein', 'German')")
                .getValue(Inventor.class);

        System.out.println(einstein.getName());
//create new inventor instance within add method of List
//        parser.parseExpression(
//                "Members.add(new org.spring.samples.spel.inventor.Inventor(
//        'Albert Einstein', 'German'))").getValue(societyContext);
    }
}
