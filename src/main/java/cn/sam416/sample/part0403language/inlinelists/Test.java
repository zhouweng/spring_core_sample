package cn.sam416.sample.part0403language.inlinelists;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import java.util.List;

// 4.3.3. Inline Lists
public class Test {
    public static void main(String[] args) {
        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = SimpleEvaluationContext.forReadOnlyDataBinding().build();
// evaluates to a Java list containing the four numbers
        List numbers = (List) parser.parseExpression("{1,2,3,4}").getValue(context);
        System.out.println("numbers.size="+numbers.size());
        for(int i=0;i<numbers.size();i++){
            System.out.println(" "+numbers.get(i));
        }

        List listOfLists = (List) parser.parseExpression("{{'a','b'},{'x','y'}}").getValue(context);
        System.out.println("listOfLists.size="+listOfLists.size());
        for(int i=0;i<listOfLists.size();i++){
            System.out.println(" "+listOfLists.get(i));
        }
    }
}
