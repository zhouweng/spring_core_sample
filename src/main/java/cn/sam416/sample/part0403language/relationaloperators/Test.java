package cn.sam416.sample.part0403language.relationaloperators;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

// 4.3.7. Operators - Relational Operators
public class Test {
    public static void main(String[] args) {

        ExpressionParser parser = new SpelExpressionParser();
        // evaluates to true
        boolean trueValue = parser.parseExpression("2 == 2").getValue(Boolean.class);
        System.out.println(trueValue);

        // evaluates to false
        boolean falseValue = parser.parseExpression("2 < -5.0").getValue(Boolean.class);
        System.out.println(falseValue);

        // evaluates to true
        trueValue = parser.parseExpression("'black' < 'block'").getValue(Boolean.class);
        System.out.println(trueValue);

        // evaluates to false
        falseValue = parser.parseExpression(
                "'xyz' instanceof T(Integer)").getValue(Boolean.class);
        System.out.println(falseValue);

        // evaluates to true
        trueValue = parser.parseExpression(
                "'5.00' matches '^-?\\d+(\\.\\d{2})?$'").getValue(Boolean.class);
        System.out.println(trueValue);

        //evaluates to false
        falseValue = parser.parseExpression(
                "'5.0067' matches '^-?\\d+(\\.\\d{2})?$'").getValue(Boolean.class);
        System.out.println(falseValue);

    }
}
