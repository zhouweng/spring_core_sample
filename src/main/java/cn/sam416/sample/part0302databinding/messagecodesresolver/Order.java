package cn.sam416.sample.part0302databinding.messagecodesresolver;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
    private Date orderDate;
    private BigDecimal orderPrice;

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }
}
