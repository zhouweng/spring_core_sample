package cn.sam416.sample.part0106customizingbean.defaultinitmethod;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDefaultInitMethod {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0106customizingbean/default-initmethod-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);

        ServiceA serviceA = context.getBean("serviceA", ServiceA.class);
        ServiceB serviceB = context.getBean("serviceB", ServiceB.class);

    }
}
