package cn.sam416.sample.part0106customizingbean.defaultinitmethod;

public class ServiceA {
    private void init() {
        // do some initialization work
        System.out.println("ServiceA.init() from default-init-method element");
    }
    private void init2() {
        // do some initialization work
        System.out.println("ServiceA.init2() from init-method element");
    }
}
