package cn.sam416.sample.part0106customizingbean.defaultinitmethod;

public class ServiceB {
    public void init() {
        // do some initialization work
        System.out.println("ServiceB.init() from default-init-method element");
    }
}
