package cn.sam416.sample.part0106customizingbean.lifecycle;

import cn.sam416.sample.part0106customizingbean.initmethod.ExampleBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestLifeCycle {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0106customizingbean/initmethod-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);

        ExampleBean exampleBean = context.getBean("exampleBean", ExampleBean.class);

    }
}
