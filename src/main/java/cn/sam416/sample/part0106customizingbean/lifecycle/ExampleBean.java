package cn.sam416.sample.part0106customizingbean.lifecycle;

import org.springframework.context.SmartLifecycle;

import javax.annotation.PostConstruct;

public class ExampleBean implements SmartLifecycle {
    @PostConstruct
    public void init() {
        // do some initialization work
        System.out.println("init() from @PostConstruct ");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void stop() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
