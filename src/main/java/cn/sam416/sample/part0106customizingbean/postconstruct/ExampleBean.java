package cn.sam416.sample.part0106customizingbean.postconstruct;

import javax.annotation.PostConstruct;

public class ExampleBean {
    @PostConstruct
    public void init() {
        // do some initialization work
        System.out.println("init() from @PostConstruct ");
    }

    @PostConstruct
    public void init2() {
        // do some initialization work
        System.out.println("init2() from @PostConstruct ");
    }
}
