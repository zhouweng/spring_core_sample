package cn.sam416.sample.part0106customizingbean.postconstruct;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestPostConstruct {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(ExampleBean.class);
        context.refresh();
    }
}
