package cn.sam416.sample.part0106customizingbean.initmethod;

public class ExampleBean {
    public void init() {
        // do some initialization work
        System.out.println("init() from init-method element");
    }
    public void init2() {
        // do some initialization work
        System.out.println("init2() from init-method element");
    }
}
