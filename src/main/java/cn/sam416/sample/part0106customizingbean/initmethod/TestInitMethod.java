package cn.sam416.sample.part0106customizingbean.initmethod;

import cn.sam416.sample.part0104dependencies.dependson.AService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestInitMethod {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0106customizingbean/initmethod-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);

        ExampleBean exampleBean = context.getBean("exampleBean", ExampleBean.class);

    }
}
