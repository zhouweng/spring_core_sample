package cn.sam416.sample.part0301validation.validator;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main (String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        Config.class);

        ClientBean bean = context.getBean(ClientBean.class);
        bean.processOrder();
    }
}
