package cn.sam416.sample.part0301validation.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.DataBinder;

import java.util.Locale;

public class ClientBean {
    @Autowired
    private Order order;

    public void processOrder () {
        if (validateOrder()) {
            System.out.println("processing " + order);
        }
    }

    private boolean validateOrder () {
        DataBinder dataBinder = new DataBinder(order);
        dataBinder.addValidators(new OrderValidator());
        dataBinder.validate();

        if (dataBinder.getBindingResult().hasErrors()) {
            ResourceBundleMessageSource messageSource =
                    new ResourceBundleMessageSource();
            messageSource.setBasename("ValidationMessages");
            System.out.println(messageSource.getMessage("order.invalid",
                    null, Locale.US));
            dataBinder.getBindingResult().getAllErrors().stream().
                    forEach(e -> System.out.println(messageSource
                            .getMessage(e, Locale.US)));
            return false;
        }
        return true;
    }
}
