package cn.sam416.sample.part0301validation.validator;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
    private Date date;
    private BigDecimal price;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
