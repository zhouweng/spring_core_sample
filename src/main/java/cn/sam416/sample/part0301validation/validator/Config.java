package cn.sam416.sample.part0301validation.validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
public class Config {

    @Bean
    public ClientBean clientBean () {
        return new ClientBean();
    }

    @Bean
    public Order order () {
        //in real scenario this order should be coming from user interface
        Order order = new Order();
        order.setPrice(BigDecimal.ZERO);
        return order;
    }
}
