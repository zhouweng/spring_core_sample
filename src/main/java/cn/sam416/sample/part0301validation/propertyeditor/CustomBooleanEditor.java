package cn.sam416.sample.part0301validation.propertyeditor;

import com.sun.beans.editors.BooleanEditor;

public class CustomBooleanEditor extends BooleanEditor {
    @Override
    public void setAsText (String text) throws IllegalArgumentException {
        if ("yes".equalsIgnoreCase(text)) {
            super.setAsText("true");
        } else if ("no".equalsIgnoreCase(text)) {
            super.setAsText("false");
        } else {
            super.setAsText(text);
        }
    }

}
