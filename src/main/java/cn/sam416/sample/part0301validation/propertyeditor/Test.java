package cn.sam416.sample.part0301validation.propertyeditor;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;

//From: https://www.logicbig.com/how-to/code-snippets/jcode-java-propertyeditor.html
public class Test {
    public static void main (String[] args) {
        //overriding default jdk boolean editor
        PropertyEditorManager.registerEditor(Boolean.class, CustomBooleanEditor.class);
        PropertyEditor editor = PropertyEditorManager.findEditor(Boolean.class);
        editor.setAsText("yes");
        System.out.println(editor.getValue());
    }
}
