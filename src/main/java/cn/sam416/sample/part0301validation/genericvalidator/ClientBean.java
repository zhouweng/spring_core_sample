package cn.sam416.sample.part0301validation.genericvalidator;

import org.springframework.beans.factory.annotation.Autowired;

public class ClientBean {
    @Autowired
    private Order order;

    @Autowired
    private GenericValidator genericValidator;

    public void processOrder () {
        if (genericValidator.validateObject(order)) {
            System.out.println("processing " + order);
        }
    }

    public Order getOrder () {
        return order;
    }
}
