package cn.sam416.sample.part0301validation.genericvalidator;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class Order {

    @NotNull(message = "{date.empty}")
    @Future(message = "{date.future}")
    private Date date;

    @NotNull(message = "{price.empty}")
    @DecimalMin(value = "0", inclusive = false, message = "{price.invalid}")
    private BigDecimal price;

    String customerId;

    public void setDate (Date date) {
        this.date = date;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public Date getDate () {
        return date;
    }

    public BigDecimal getPrice () {
        return price;
    }

    public String getCustomerId () {
        return customerId;
    }

    public void setCustomerId (String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString () {
        return "Order{ date=" + date + ", price=" + price +
                ", customerId='" + customerId + "'}";
    }
}
