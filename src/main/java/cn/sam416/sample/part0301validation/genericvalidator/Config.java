package cn.sam416.sample.part0301validation.genericvalidator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class Config {
    @Bean
    public ClientBean clientBean () {
        return new ClientBean();
    }

    @Bean
    public Order order () {
        Order order = new Order();
        //  order.setPrice(BigDecimal.TEN);
        //  order.setDate(new Date(System.currentTimeMillis() + 100000));
        order.setCustomerId("111");
        return order;
    }

    @Bean
    public org.springframework.validation.Validator validatorFactory () {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public OrderValidator orderValidator () {
        return new OrderValidator();
    }

    @Bean
    public GenericValidator genericValidator () {
        return new GenericValidator();
    }
}
