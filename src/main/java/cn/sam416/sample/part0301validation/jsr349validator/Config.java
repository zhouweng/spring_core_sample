package cn.sam416.sample.part0301validation.jsr349validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;
import java.util.Date;


@Configuration
public  class Config {

    @Bean
    public ClientBean clientBean () {
        return new ClientBean();
    }

    @Bean
    public Order order () {

        Order order = new Order();
        // order.setPrice(BigDecimal.TEN);
        order.setDate(new Date(System.currentTimeMillis() - 60000));
        return order;
    }

    @Bean
    public Validator validatorFactory () {
        return new LocalValidatorFactoryBean();
    }
}

