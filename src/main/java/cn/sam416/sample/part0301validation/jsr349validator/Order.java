package cn.sam416.sample.part0301validation.jsr349validator;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class Order {
    @NotNull(message = "{date.empty}")
    @Future(message = "{date.future}")
    private Date date;

    @NotNull(message = "{price.empty}")
    @DecimalMin(value = "0", inclusive = false, message = "{price.invalid}")
    private BigDecimal price;

    public void setDate (Date date) {
        this.date = date;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public Date getDate () {
        return date;
    }

    public BigDecimal getPrice () {
        return price;
    }

    @Override
    public String toString () {
        return "Order{'date='" + date + "', price=" + price + '}';
    }
}
