package cn.sam416.sample.part0301validation.jsr349validator;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//From: https://www.logicbig.com/tutorials/spring-framework/spring-core/core-validation.html
public class Test {

    public static void main (String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        Config.class);

        ClientBean bean = context.getBean(ClientBean.class);
        bean.processOrder();
    }
}
