package cn.sam416.sample.part0301validation.jsr349validator;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Locale;
import java.util.Set;

public class ClientBean {

    @Autowired
    private Order order;
    @Autowired
    Validator validator;

    public void processOrder () {
        if (validateOrder()) {
            System.out.println("processing " + order);
        }
    }

    private boolean validateOrder () {
        Locale.setDefault(Locale.US);
        Set<ConstraintViolation<Order>> c = validator.validate(order);
        if (c.size() > 0) {
            System.out.println("Order validation errors:");
            c.stream().map(v -> v.getMessage())
                    .forEach(System.out::println);
            return false;
        }
        return true;
    }
}
