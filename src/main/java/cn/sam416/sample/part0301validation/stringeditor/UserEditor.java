package cn.sam416.sample.part0301validation.stringeditor;

import com.sun.beans.editors.StringEditor;

public class UserEditor extends StringEditor {
    @Override
    public String getAsText () {
        User user = (User) getValue();
        return user.getName();
    }

    @Override
    public void setAsText (String s) {
        User user = new User();
        user.setName("Hello "+s);
        setValue(user);
    }
}
