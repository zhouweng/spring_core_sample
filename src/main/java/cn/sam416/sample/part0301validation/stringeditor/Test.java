package cn.sam416.sample.part0301validation.stringeditor;

import org.springframework.beans.propertyeditors.CustomBooleanEditor;

import java.beans.*;

//From: https://www.logicbig.com/how-to/code-snippets/jcode-java-propertyeditor.html
public class Test {
    public static void main (String[] args) {
        PropertyEditor editor = PropertyEditorManager.findEditor(User.class);
        editor.setAsText("Joe");
        System.out.println(editor.getValue());
        
    }

}
