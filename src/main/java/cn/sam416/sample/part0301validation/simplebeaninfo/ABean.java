package cn.sam416.sample.part0301validation.simplebeaninfo;

public class ABean {
    private String someStr;

    public String getSomeStr () {
        return someStr;
    }

    public void setSomeStr (String someStr) {
        this.someStr = someStr;
    }
}
