package cn.sam416.sample.part0301validation.simplebeaninfo;

import java.beans.*;

public class Test {
    public static void main (String[] args) throws IntrospectionException {
        //auto discovering ABeanBeanInfo
        BeanInfo beanInfo = Introspector.getBeanInfo(ABean.class);
        System.out.println(beanInfo.getClass());
        System.out.println(beanInfo.getBeanDescriptor()
                .getDisplayName());
    }

}
