package cn.sam416.sample.part0301validation.simplebeaninfo;

import java.beans.BeanDescriptor;
import java.beans.SimpleBeanInfo;

public class ABeanBeanInfo extends SimpleBeanInfo {
    @Override
    public BeanDescriptor getBeanDescriptor () {
        BeanDescriptor bd = new BeanDescriptor(ABean.class);
        bd.setDisplayName("A Bean");
        return bd;
    }
}
