package cn.sam416.sample.part0601pointcut.jdkregexpmethodpointcut;

public class Person {
    public void Say( String msg) {
        System.out.println("Saying :" + msg);
    }

    public void Run () {
        System.out.println("Running");
    }

    public void Die() {
        System.out.println("I'm dead");
    }
}
