package cn.sam416.sample.part0601pointcut.jdkregexpmethodpointcut;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class LoggerPerson implements MethodBeforeAdvice {

    private static final Log log = LogFactory.getLog(LoggerPerson.class);

    public void before(Method method, Object[] args, Object target)
            throws Throwable {
        // TODO Auto-generated method stub
        log.info(target.getClass().getSimpleName() + " invocation " + method.getName());
    }

}
