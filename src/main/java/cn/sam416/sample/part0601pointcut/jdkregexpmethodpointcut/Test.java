package cn.sam416.sample.part0601pointcut.jdkregexpmethodpointcut;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
// From: http://makble.com/spring-jdkregexpmethodpointcut-pointcut-example
// 6.1. Pointcut API in Spring
public class Test {
    public static void main (String [] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("cn/sam416/sample/part0601pointcut/jdk-pointcut.xml");
        Person p = (Person)context.getBean("ProxyFactoryBean");
        p.Say("hello");
        p.Die();
        p.Run();

    }
}
