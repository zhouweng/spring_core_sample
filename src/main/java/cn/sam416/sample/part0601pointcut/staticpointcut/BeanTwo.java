package cn.sam416.sample.part0601pointcut.staticpointcut;

public class BeanTwo {
    public void foo() {
        System.out.println("foo");
    }

    public void bar() {
        System.out.println("bar");
    }
}
