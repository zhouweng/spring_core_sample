package cn.sam416.sample.part0601pointcut.regexpmethodpointcutadvisor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
// From: https://ramj2ee.blogspot.com/2018/11/spring-aop-example-how-to-apply.html
// 6.1. Pointcut API in Spring
public class Test {
    public static void main(String[] args)
    {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "cn/sam416/sample/part0601pointcut/regexp-pointcut.xml");
        System.out.println("---------------------------------------");
        EmployeeService employeeService = context
                .getBean("employeeServiceProxy", EmployeeService.class);
        employeeService.getEmployeeName();
        System.out.println("----------------------------------------");
        employeeService.getEmployeeAge();
        System.out.println("----------------------------------------");
        employeeService.getEmployeeAddress();
    }
}
