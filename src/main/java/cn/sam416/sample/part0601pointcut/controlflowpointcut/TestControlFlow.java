package cn.sam416.sample.part0601pointcut.controlflowpointcut;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.ControlFlowPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
public class TestControlFlow {
    public void test() {
        SimpleClass target = new SimpleClass();
        Pointcut pointcut = new ControlFlowPointcut(TestControlFlow.class,
                "controlFlowTest");
        Advice advice = new TestAdvice();
        ProxyFactory proxyFactory = new ProxyFactory();

        Advisor advisor = new DefaultPointcutAdvisor(pointcut, advice);
        proxyFactory.addAdvisor(advisor);
        proxyFactory.setTarget(target);
        SimpleClass simpleProxy = (SimpleClass) proxyFactory.getProxy();
        System.out.println("Calling Normally");
        simpleProxy.sayHi();
        System.out.println("Calling in ControlFlow");
        controlFlowTest(simpleProxy);
    }

    public void controlFlowTest(SimpleClass simpleClass) {
        simpleClass.sayHi();
    }

}
