package cn.sam416.sample.part0601pointcut.controlflowpointcut;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class TestAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] boObjects, Object object)
            throws Throwable {
        // TODO Auto-generated method stub
        System.out.println("Calling before " + method);
    }

}
