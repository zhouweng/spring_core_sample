package cn.sam416.sample.part0601pointcut.controlflowpointcut;
// From: http://www.java2s.com/Code/Java/Spring/ControlFlowExample.htm
// 6.1. Pointcut API in Spring
public class Test {
    public static void main(String[] args) {
        TestControlFlow testControlFlow = new TestControlFlow();
        testControlFlow.test();
    }
}
