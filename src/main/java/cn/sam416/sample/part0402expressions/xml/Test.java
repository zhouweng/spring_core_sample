package cn.sam416.sample.part0402expressions.xml;

import com.sun.javafx.runtime.SystemProperties;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: 4.2.1. XML Configuration
public class Test {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0402expressions/hello_bean.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        NumberGuess numberGuess = context.getBean("numberGuess", NumberGuess.class);
        System.out.println("this's numberGuess.randomNumber "+numberGuess.getRandomNumber());

        ShapeGuess shapeGuess = context.getBean("shapeGuess", ShapeGuess.class);
        System.out.println("this's shapeGuess.shapeSeed "+shapeGuess.getInitialShapeSeed());


    }
}
