package cn.sam416.sample.part0402expressions.xml;

public class ShapeGuess {
    String initialShapeSeed;

    public String getInitialShapeSeed() {
        return initialShapeSeed;
    }

    public void setInitialShapeSeed(String initialShapeSeed) {
        this.initialShapeSeed = initialShapeSeed;
    }
}
