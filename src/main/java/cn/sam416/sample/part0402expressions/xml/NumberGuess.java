package cn.sam416.sample.part0402expressions.xml;

public class NumberGuess {
    double randomNumber;

    public double getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(double randomNumber) {
        this.randomNumber = randomNumber;
    }
}
