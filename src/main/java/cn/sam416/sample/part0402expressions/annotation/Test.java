package cn.sam416.sample.part0402expressions.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

//From: 4.2.2. Annotation Configuration
public class Test {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("cn.sam416.sample.part0402expressions.annotation");
        NumberGuess numberGuess = context.getBean(NumberGuess.class);
        System.out.println(numberGuess.getRandomNumber());
    }
}
