package cn.sam416.sample.part0402expressions.annotation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NumberGuess {
    @Value("#{ T(java.lang.Math).random() * 100.0 }")
    double randomNumber;

    public double getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(double randomNumber) {
        this.randomNumber = randomNumber;
    }
}
