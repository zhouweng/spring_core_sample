package cn.sam416.sample.part0303converter.conditionalgenericconverter;

import org.springframework.core.convert.support.DefaultConversionService;
import java.math.BigDecimal;

public class Test {

    public static void main (String[] args) {
        DefaultConversionService service = new DefaultConversionService();
        service.addConverter(new NumberToBigDecimalConverter());
        BigDecimal bd = service.convert(Double.valueOf("2222.336"),
                BigDecimal.class);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println(bd);

        //this will return the same BigDecimal instance without conversion
        bd = service.convert(new BigDecimal("898.33"), BigDecimal.class);
        System.out.println(bd);

        bd = service.convert(new Integer("666"), BigDecimal.class);
        System.out.println(bd);
    }

}
