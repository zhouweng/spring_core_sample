package cn.sam416.sample.part0303converter.dateformat;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class Test {
    public static void main(String[] args) {

        Date curDate = new Date();

        System.out.println("ROW="+curDate.toString());

        String DateToStr = DateFormat.getInstance().format(curDate);
        System.out.println("getInstance()="+DateToStr);

        DateToStr = DateFormat.getTimeInstance().format(curDate);
        System.out.println("getTimeInstance()="+DateToStr);

        DateToStr = DateFormat.getDateInstance().format(curDate);
        System.out.println("getDateInstance()="+DateToStr);

        DateToStr = DateFormat.getDateTimeInstance().format(curDate);
        System.out.println("getDateTimeInstance()="+DateToStr);

        DateToStr = DateFormat.getTimeInstance(DateFormat.SHORT)
                .format(curDate);
        System.out.println("getTimeInstance(DateFormat.SHORT)="+DateToStr);

        DateToStr = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(
                curDate);
        System.out.println("getTimeInstance(DateFormat.MEDIUM)="+DateToStr);

        DateToStr = DateFormat.getTimeInstance(DateFormat.LONG).format(curDate);
        System.out.println("getTimeInstance(DateFormat.LONG)="+DateToStr);

        DateToStr = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(curDate);
        System.out.println("getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT)="+DateToStr);

        try {
            Date strToDate = DateFormat.getDateInstance()
                    .parse("2014-05-19");
            System.out.println("getDateInstance().parse="+strToDate.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
