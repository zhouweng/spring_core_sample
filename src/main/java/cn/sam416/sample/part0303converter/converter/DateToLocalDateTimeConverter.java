package cn.sam416.sample.part0303converter.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateToLocalDateTimeConverter implements Converter<Date, LocalDateTime> {

    @Override
    public LocalDateTime convert (Date source) {
        return LocalDateTime.ofInstant(source.toInstant(),
                ZoneId.systemDefault());
    }
}
