package cn.sam416.sample.part0303converter.converter;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//From: https://www.logicbig.com/how-to/code-snippets/jcode-spring-framework-converter.html
public class Test {
    public static void main (String[] args) {
        AnnotationConfigApplicationContext context = new
                AnnotationConfigApplicationContext(
                Config.class);

        ClientBean clientBean = context.getBean(ClientBean.class);
        clientBean.showLocalDateTime();
    }
}
