package cn.sam416.sample.part0303converter.localvalidatorfactorybean;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class Test {
    public static void main (String[] args) {

        AnnotationConfigApplicationContext context = new
                AnnotationConfigApplicationContext(
                Config.class);

        ClientBean clientBean = context.getBean(ClientBean.class);
        clientBean.processOrder();
    }





}
