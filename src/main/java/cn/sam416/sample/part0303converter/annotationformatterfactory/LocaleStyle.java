package cn.sam416.sample.part0303converter.annotationformatterfactory;

public enum LocaleStyle {
    CountryDisplayName,
    ISO3Country,
    ISO3Language
}
