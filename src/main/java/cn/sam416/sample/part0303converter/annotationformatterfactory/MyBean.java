package cn.sam416.sample.part0303converter.annotationformatterfactory;

import java.util.Locale;

public class MyBean {
    @LocaleFormat(style = LocaleStyle.ISO3Language)
    private Locale myLocale;

    public Locale getMyLocale () {
        return myLocale;
    }

    public void setMyLocale (Locale myLocale) {
        this.myLocale = myLocale;
    }
}
