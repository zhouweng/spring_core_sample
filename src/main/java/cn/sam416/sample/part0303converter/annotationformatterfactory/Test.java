package cn.sam416.sample.part0303converter.annotationformatterfactory;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.validation.DataBinder;

// From: https://www.logicbig.com/how-to/code-snippets/jcode-spring-framework-annotationformatterfactory.html
public class Test {
    public static void main (String[] args) {
        DefaultFormattingConversionService service =
                new DefaultFormattingConversionService();
        service.addFormatterForFieldAnnotation(
                new LocaleFormatAnnotationFormatterFactory());

        MyBean bean = new MyBean();
        DataBinder dataBinder = new DataBinder(bean);
        dataBinder.setConversionService(service);

        MutablePropertyValues mpv = new MutablePropertyValues();
        mpv.add("myLocale", "msa");

        dataBinder.bind(mpv);
        dataBinder.getBindingResult()
                .getModel()
                .entrySet()
                .forEach(System.out::println);
        System.out.println(bean.getMyLocale().getDisplayCountry());
    }
}
