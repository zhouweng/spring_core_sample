package cn.sam416.sample.part0303converter.numberformat;


import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
//From: http://tutorials.jenkov.com/java-internationalization/numberformat.html
public class Test {
    public static void main (String[] args) {
        Locale locale = new Locale("da", "DK");
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        String number = numberFormat.format(100.99);
        System.out.println(number);

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
        String currency = currencyFormat.format(100.999);
        System.out.println(currency);

        NumberFormat percentageFormat = NumberFormat.getPercentInstance(locale);
        String percentage = percentageFormat.format(99.999);
        System.out.println(percentage);

        numberFormat.setRoundingMode(RoundingMode.HALF_DOWN);
        numberFormat.setMinimumFractionDigits(0);
        numberFormat.setMaximumFractionDigits(0);
        number = numberFormat.format(99.50);
        System.out.println(number);

        Number parse = null;
        try {
            parse = numberFormat.parse("100,00");
            System.out.println(parse.intValue());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
