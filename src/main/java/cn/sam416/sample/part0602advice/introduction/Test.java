package cn.sam416.sample.part0602advice.introduction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
// From: http://www.java2s.com/Code/Java/Spring/IntroductionConfigExample.htm
public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new FileSystemXmlApplicationContext(
                "classpath:/cn/sam416/sample/part0602advice/introductions.xml");

        TargetBean bean = (TargetBean) ctx.getBean("bean");
        IsModified mod = (IsModified) bean;

        // test interfaces
        System.out.println("Is TargetBean?: " + (bean instanceof TargetBean));
        System.out.println("Is IsModified?: " + (bean instanceof IsModified));

        // test is modified implementation
        System.out.println("Has been modified?: " + mod.isModified());
        bean.setName("Rob Harrop");
        System.out.println("Has been modified?: " + mod.isModified());
        bean.setName("Joe Schmoe");
        System.out.println("Has been modified?: " + mod.isModified());
    }
}
