package cn.sam416.sample.part0602advice.introduction;

public interface IsModified {

    public boolean isModified();
}
