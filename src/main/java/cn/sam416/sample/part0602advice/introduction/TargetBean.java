package cn.sam416.sample.part0602advice.introduction;

public class TargetBean {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
