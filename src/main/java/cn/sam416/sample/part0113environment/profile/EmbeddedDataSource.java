package cn.sam416.sample.part0113environment.profile;

public class EmbeddedDataSource implements DataSource {
    @Override
    public void say() {
        System.out.println("EmbeddedDataSource.say()");
    }
}
