package cn.sam416.sample.part0113environment.profile;

import cn.sam416.sample.part0112javabased.construction.AppConfig;
import cn.sam416.sample.part0112javabased.construction.MyService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext  ctx = new AnnotationConfigApplicationContext();
        ctx.getEnvironment().setActiveProfiles("development");
        ctx.register(StandaloneDataConfig.class, JndiDataConfig.class);
        ctx.refresh();
        DataSource dataSourceDev = ctx.getBean(DataSource.class);
        dataSourceDev.say();

//        ctx.getEnvironment().setActiveProfiles("production");
//        ctx.register(StandaloneDataConfig.class, JndiDataConfig.class);
//        //@1 GenericApplicationContext does not support multiple refresh attempts: just call 'refresh' once
//        ctx.refresh();
//        DataSource dataSourceProd = ctx.getBean(DataSource.class);
//        dataSourceProd.say();
    }
}
