package cn.sam416.sample.part0113environment.property;

import cn.sam416.sample.part0113environment.profile.DataSource;
import cn.sam416.sample.part0113environment.profile.JndiDataConfig;
import cn.sam416.sample.part0113environment.profile.StandaloneDataConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext  ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        TestBean testBean = ctx.getBean(TestBean.class);
        testBean.say();
    }
}
