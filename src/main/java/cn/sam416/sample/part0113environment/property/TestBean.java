package cn.sam416.sample.part0113environment.property;

public class TestBean {
    String name;

    public void setName(String name) {
        this.name = name;
    }

    public void say() {
        System.out.println("my name is "+name);
    }
}
