package cn.sam416.sample.part0115applicationcontext.applicationeventpublisheraware;

public class MyEvent {
    private final String msg;

    public MyEvent (String msg) {
        this.msg = msg;
    }

    public String getMsg () {
        return msg;
    }
}
