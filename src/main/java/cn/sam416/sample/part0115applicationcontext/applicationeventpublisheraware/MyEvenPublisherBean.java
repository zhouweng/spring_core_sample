package cn.sam416.sample.part0115applicationcontext.applicationeventpublisheraware;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class MyEvenPublisherBean implements ApplicationEventPublisherAware {
    ApplicationEventPublisher publisher;

    public void sendMsg (String msg) {
        publisher.publishEvent(new MyEvent(msg));
    }

    @Override
    public void setApplicationEventPublisher (
            ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
