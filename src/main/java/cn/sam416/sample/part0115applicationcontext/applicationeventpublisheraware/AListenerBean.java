package cn.sam416.sample.part0115applicationcontext.applicationeventpublisheraware;

import org.springframework.context.event.EventListener;

public class AListenerBean {
    @EventListener
    public void onMyEvent (MyEvent event) {
        System.out.print("event received: " + event.getMsg());
    }
}
