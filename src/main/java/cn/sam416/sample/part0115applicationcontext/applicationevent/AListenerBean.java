package cn.sam416.sample.part0115applicationcontext.applicationevent;

import org.springframework.context.event.EventListener;

public class AListenerBean {
    @EventListener
    public void onMyEvent (MyEvent event) {
        System.out.print("event received: "+event.getMsg());
        System.out.println(" -- source: "+event.getSource());
    }
}
