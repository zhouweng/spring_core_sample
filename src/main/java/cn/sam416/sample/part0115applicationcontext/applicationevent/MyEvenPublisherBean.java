package cn.sam416.sample.part0115applicationcontext.applicationevent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

public class MyEvenPublisherBean {
    @Autowired
    ApplicationEventPublisher publisher;

    public void sendMsg(String msg){
        publisher.publishEvent(new MyEvent(this, msg));
    }
}
