package cn.sam416.sample.part0115applicationcontext.contextrefreshedevent;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

//From: https://www.logicbig.com/how-to/code-snippets/jcode-spring-framework-applicationlistener.html
//Maybe need airport
@Configuration
public class Test {
    @Bean
    AListenerBean listenerBean () {
        System.out.println("listenerBean() start");
        return new AListenerBean();
    }

    public static void main (String[] args) {
        System.out.println("main() start");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                Test.class);
        System.out.println("main() end");
    }

    private static class AListenerBean implements ApplicationListener<ContextRefreshedEvent> {
        @Override
        public void onApplicationEvent (ContextRefreshedEvent event) {
            System.out.print("context refreshed event fired: ");
            System.out.println(event);
        }
    }
}
