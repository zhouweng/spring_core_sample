package cn.sam416.sample.part0115applicationcontext.exceptions;

import cn.sam416.sample.part0102helloworld.helloworld.HelloWorld;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Locale;

public class Test {
    public static void main(String[] args) {

        MessageSource resources = new ClassPathXmlApplicationContext("classpath:/cn/sam416/sample/part0115applicationcontext/exceptions-beans.xml");
        String message = resources.getMessage("argument.required",
                new Object [] {"userDao"}, "Required", Locale.UK);
        System.out.println(message);
    }
}
