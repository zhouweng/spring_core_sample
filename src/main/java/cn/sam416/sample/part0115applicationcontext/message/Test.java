package cn.sam416.sample.part0115applicationcontext.message;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Locale;

public class Test {
    public static void main(String[] args) {
        MessageSource resources = new ClassPathXmlApplicationContext("classpath:/cn/sam416/sample/part0115applicationcontext/message-bean.xml");
        String message = resources.getMessage("message", null, "Default", Locale.ENGLISH);
        System.out.println(message);

    }
}
