package cn.sam416.sample.part0105scopes.prototype;

import cn.sam416.sample.part0104dependencies.dependson.AService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestPrototype {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0105scopes/prototype-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);

        AService aService1 = context.getBean("aService", AService.class);
        AService aService2 = context.getBean("aService", AService.class);
        System.out.println(aService1);
        System.out.println(aService2);

    }
}
