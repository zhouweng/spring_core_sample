package cn.sam416.sample.part0105scopes.thread;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("threadScope")
@Service
public class MessageServiceImpl implements MessageService  {

    public String getMessage() {
        return "Hello World!";
    }

}
