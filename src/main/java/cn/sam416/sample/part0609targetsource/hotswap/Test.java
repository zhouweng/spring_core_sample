package cn.sam416.sample.part0609targetsource.hotswap;

import cn.sam416.sample.part0608autoproxy.beannameautoproxy.EmployeBean;
import org.springframework.aop.target.HotSwappableTargetSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://wiki.scn.sap.com/wiki/display/Java/An+example+of+using+HotSwappableTargetSource
public class Test {
    public static void main(String args[]) {

        ApplicationContext context = new ClassPathXmlApplicationContext(
                new String[] { "cn/sam416/sample/part0609targetsource/hotswap-bean.xml" });

        HotSwappableTargetSource swapper = (HotSwappableTargetSource) context.getBean("swapper");
        Object swapable = context.getBean("swappable");
        Durid level1Durid = (Durid)swapable;
        level1Durid.castStorm(); // level 1 storm is casted.
// The magic occurs here: after the above swap is called, the proxy level1Durid now points to the level 10 Durid
        swapper.swap(new Level10Durid());
        level1Durid.castStorm(); // level 10 storm is called.

    }
}
