package cn.sam416.sample.part0104dependencies.pnamespace;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor-based Dependency Injection
public class TestPNamespace {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/pnamespace-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        Person john_classic = context.getBean("john-classic", Person.class);
        Person john_modern = context.getBean("john-modern", Person.class);

        System.out.println(john_classic);
        System.out.println(john_modern);
    }
}
