package cn.sam416.sample.part0104dependencies.replacedmethod;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class ReplaceMethodTest {
    public static void main(String[] args) {
        String pathA = "classpath:/cn/sam416/sample/part0104dependencies/replace_method_bean.xml";
        String[] path = new String[] {pathA};
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(path);
        //
        PersonA_use_lookup_method persona =
                (PersonA_use_lookup_method) context.getBean("persona");

        System.out.println("++++++++");
        System.out.println(persona.getAFFruit());
        System.out.println("++++++++");
        System.out.println(persona.getAFFruit(""));
        System.out.println("++++++++");
        System.out.println(persona.getAFFruit((List<?>)null));
        System.out.println("++++++++");

    }
}
