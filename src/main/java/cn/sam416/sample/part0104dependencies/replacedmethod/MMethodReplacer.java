package cn.sam416.sample.part0104dependencies.replacedmethod;

import org.springframework.beans.factory.support.MethodReplacer;

import java.lang.reflect.Method;
import java.util.UUID;

public class MMethodReplacer implements MethodReplacer {
    public Object reimplement(Object obj, Method method, Object[] args) throws Throwable {
        return "String-Obj:from method-replacer-" + UUID.randomUUID();
    }
}
