package cn.sam416.sample.part0104dependencies.replacedmethod;


import java.util.List;

public class PersonA_use_lookup_method {
    public Object getAFFruit() {
        System.out.println("invoke 0");
        return null;
    }
    public Object getAFFruit(String str) {
        System.out.println("invoke string");
        return null;
    }
    public Object getAFFruit(List<?> list) {
        System.out.println("invoke list");
        return null;
    }
}

