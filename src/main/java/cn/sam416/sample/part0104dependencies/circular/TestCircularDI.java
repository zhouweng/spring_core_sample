package cn.sam416.sample.part0104dependencies.circular;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor-based Dependency Injection
public class TestCircularDI {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/circular-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        //@1 will throw exception :
        // BeanCurrentlyInCreationException: Error creating bean with name 'aService':
        // Requested bean is currently in creation: Is there an unresolvable circular reference?
        AService aService = context.getBean("aService", AService.class);
    }
}
