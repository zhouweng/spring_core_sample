package cn.sam416.sample.part0104dependencies.constructor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor-based Dependency Injection
public class TestConstructorDI {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/constructor-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        SimpleMovieLister simpleMovieLister = context.getBean("simpleMovieLister", SimpleMovieLister.class);

    }
}
