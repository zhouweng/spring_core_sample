package cn.sam416.sample.part0104dependencies.argument;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor argument type matching
public class TestConstructorArgument {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/argument-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        ExampleBean exampleBean = context.getBean("exampleBean", ExampleBean.class);

        System.out.println(exampleBean);
    }
}
