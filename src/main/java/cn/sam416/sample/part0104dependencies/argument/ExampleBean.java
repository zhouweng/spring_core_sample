package cn.sam416.sample.part0104dependencies.argument;

import java.beans.ConstructorProperties;

public class ExampleBean {
    int years;
    String ultimateAnswer;
// Try it: If there is new parameter [month], how to change this file ?

    // @3  @ConstructorProperties({"year03", "ultimateAnswer03"})
    public ExampleBean(int years, String ultimateAnswer) {
        this.years = years;
        this.ultimateAnswer = ultimateAnswer;
    }

    @Override
    public String toString() {
        return "ExampleBean{" +
                "years=" + years +
                ", ultimateAnswer='" + ultimateAnswer + '\'' +
                '}';
    }
}
