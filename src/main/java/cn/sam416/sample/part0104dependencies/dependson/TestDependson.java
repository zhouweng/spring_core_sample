package cn.sam416.sample.part0104dependencies.dependson;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor-based Dependency Injection
public class TestDependson {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/dependson-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);

        AService aService = context.getBean("aService", AService.class);
    }
}
