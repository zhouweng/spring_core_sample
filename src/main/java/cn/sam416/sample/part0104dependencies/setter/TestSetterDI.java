package cn.sam416.sample.part0104dependencies.setter;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor argument type matching
public class TestSetterDI {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/setter-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        SimpleMovieLister simpleMovieLister = context.getBean("simpleMovieLister", SimpleMovieLister.class);

    }
}
