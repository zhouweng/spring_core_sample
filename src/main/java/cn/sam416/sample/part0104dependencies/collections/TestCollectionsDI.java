package cn.sam416.sample.part0104dependencies.collections;

import org.springframework.context.support.ClassPathXmlApplicationContext;

// Constructor argument type matching
public class TestCollectionsDI {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/collections-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        ComplexObject moreComplexObject = context.getBean("moreComplexObject", ComplexObject.class);

        for (String name : moreComplexObject.getAdminEmails().stringPropertyNames()) {
            System.out.println(moreComplexObject.getAdminEmails().getProperty(name));
        }
        for (Object name : moreComplexObject.getSomeList()) {
            System.out.println(name);
        }
    }
}
