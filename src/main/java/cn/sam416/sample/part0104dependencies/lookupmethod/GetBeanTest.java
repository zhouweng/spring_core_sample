package cn.sam416.sample.part0104dependencies.lookupmethod;

public abstract class GetBeanTest {
    public void who() {
        this.getBean().who();
    }

    public abstract Player getBean();
}
