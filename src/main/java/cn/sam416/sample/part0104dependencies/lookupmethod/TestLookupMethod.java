package cn.sam416.sample.part0104dependencies.lookupmethod;

import org.springframework.context.support.ClassPathXmlApplicationContext;

//From: https://blog.csdn.net/qq_22912803/article/details/52503914
public class TestLookupMethod {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/lookup_method_bean.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        GetBeanTest getBeanTest = (GetBeanTest) context.getBean("getYaoBean");
        getBeanTest.who();
        getBeanTest = (GetBeanTest)context.getBean("getWangBean");
        getBeanTest.who();
    }
}
