package cn.sam416.sample.part0104dependencies.lazy;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestLazy {
    public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0104dependencies/lazy-beans.xml";
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(beanXml);
        print_beans("Stage1",context);
        AnotherBean anotherBean = context.getBean("not.lazy", AnotherBean.class);
        print_beans("Stage2",context);
        ExpensiveToCreateBean expensiveToCreateBean = context.getBean("lazy", ExpensiveToCreateBean.class);
        print_beans("Stage3",context);
    }

    private static void print_beans (String stage, ClassPathXmlApplicationContext context) {
        System.out.println("===="+stage+"====");
        System.out.println(context.getBeanFactory().getSingletonCount());
    }
}
