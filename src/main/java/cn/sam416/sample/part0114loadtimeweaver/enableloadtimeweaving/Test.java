package cn.sam416.sample.part0114loadtimeweaver.enableloadtimeweaving;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

// https://www.concretepage.com/spring/enableloadtimeweaving-spring-example
// maybe need airport
public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AspectConfig.class);
        ctx.refresh();
        UserService userService = ctx.getBean(UserService.class);
        userService.doTask();
    }
}
