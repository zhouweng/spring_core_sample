package cn.sam416.sample.part02resource;

import org.springframework.core.io.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

// From: https://www.cnblogs.com/deityjian/p/11487644.html
// including UrlResource, ByteArrayResource, InputStreamResource, FileSystemResource
public class ResourceTest {
    public static void main(String[] args) {
        Resource resourceTxt = new FileSystemResource("D:\\SpringCore.txt");
        System.out.println(resourceTxt.getFilename());

        try {
            Resource resourceUrl = new UrlResource("http://docs.spring.io/spring/docs/4.0.0.M1/spring-framework-reference/pdf/spring-framework-reference.pdf");
            System.out.println(resourceUrl.getFilename());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        ByteArrayResource resourceByteArray = new ByteArrayResource("Hello".getBytes());
        try {
            System.out.println(resourceByteArray.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream is = new FileInputStream("D:\\SpringCore.txt");
            InputStreamResource resourceInputStream = new InputStreamResource(is);
            //对于InputStreamResource而言，其getInputStream()方法只能调用一次，继续调用将抛出异常。
            InputStream is2 = null;   //返回的就是构件时的那个InputStream
            is2 = resourceInputStream.getInputStream();
            System.out.println(is2);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Spring框架为了更方便的获取资源，尽量弱化程序员对各个Resource接口的实现类的感知，定义了另一个ResourceLoader接口。
        // 该接口的getResource(String location)方法可以用来获取资源。
        // 它的DefaultResourceLoader实现类可以适用于所有的环境，可以返回ClassPathResource、UrlResource等。
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resourceLoader = loader.getResource("http://www.baidu.com");
        System.out.println(resourceLoader instanceof UrlResource); //true
        resourceLoader = loader.getResource("classpath:Test.txt");
        System.out.println(resourceLoader instanceof ClassPathResource); //true
        resourceLoader = loader.getResource("test.txt");
        System.out.println(resourceLoader instanceof ClassPathResource); //true


        try {

            Resource resource = new FileSystemResource("D:\\SpringCore.txt");
//            Resource resource = new ClassPathResource("classpath:/cn/sam416/sample/part02resource/Test.txt");
            //判断文件是否存在：
            if (resource.exists()) {
                System.out.println("文件存在");
            }
            //判断资源文件是否可读
            if (resource.isReadable()) {
                System.out.println("文件可读");
            }
            //判断当前Resource代表的底层资源是否已经打开
            if (resource.isOpen()) {
                System.out.println("资源文件已打开");
            }
            System.out.println(resource.getURL());//获取资源所在的URL
            System.out.println(resource.getURI());//获取资源所在的URI
            resource.getFile();//返回当前资源对应的File。
            System.out.println(resource.contentLength());//输出内容长度
            System.out.println(resource.lastModified());//返回当前Resource代表的底层资源的最后修改时间。
            resource.createRelative("MyFile");//根据资源的相对路径创建新资源。[默认不支持创建相对路径资源]
            System.out.println(resource.getFilename());//获取资源文件名
            System.out.println(resource.getDescription());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
