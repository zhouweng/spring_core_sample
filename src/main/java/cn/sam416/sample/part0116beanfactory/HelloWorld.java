package cn.sam416.sample.part0116beanfactory;

public class HelloWorld {
    String name="Spring";

    public void say() {
        System.out.println("Hello world "+name);
    }
}
