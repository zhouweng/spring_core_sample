package cn.sam416.sample.part0116beanfactory;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

        public static void main(String[] args) {
        String beanXml = "classpath:/cn/sam416/sample/part0116beanfactory/hello_bean.xml";
        BeanFactory context = new ClassPathXmlApplicationContext(beanXml);
        HelloWorld helloWorld = context.getBean("helloWorld", HelloWorld.class);
        helloWorld.say();

    }
}
