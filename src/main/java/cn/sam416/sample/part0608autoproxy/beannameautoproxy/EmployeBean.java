package cn.sam416.sample.part0608autoproxy.beannameautoproxy;

public class EmployeBean {
    String empName;
    int empId;

    public EmployeBean() {

    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public EmployeBean(String empName, int empId) {
        this.empName = empName;
        this.empId = empId;
    }

    public void showDetail() {
        System.out.println("Name " + this.empName + " Id " + this.empId);
    }
}
