package cn.sam416.sample.part0608autoproxy.beannameautoproxy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//From: https://www.roseindia.net/tutorial/spring/spring3/aop/springaopbeannameautoproxy.html
public class Test {
    public static void main(String args[]) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                new String[] { "cn/sam416/sample/part0608autoproxy/beanname-autoproxy.xml" });
        EmployeBean employeBean = (EmployeBean) applicationContext
                .getBean("employeBean");
        employeBean.showDetail();

    }
}
