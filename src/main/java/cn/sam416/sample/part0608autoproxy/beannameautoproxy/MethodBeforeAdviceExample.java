package cn.sam416.sample.part0608autoproxy.beannameautoproxy;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class MethodBeforeAdviceExample implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] objects, Object object)
            throws Throwable {
        // TODO Auto-generated method stub
        System.out.println("Inside Method Before Advice");
        System.out.println("Executing " + method.getName() + "() Method");
    }

}
