package cn.sam416.sample.part0112javabased.dependencies;

public interface TransferService {
    void transfer(double amnt,String accountA, String accountB);
}
