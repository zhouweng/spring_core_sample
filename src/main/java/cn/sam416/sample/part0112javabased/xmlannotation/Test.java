package cn.sam416.sample.part0112javabased.xmlannotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/cn/sam416/sample/part0112javabased/xmlannotation.xml");
        MyService myService = ctx.getBean(MyService.class);
        myService.doStuff();
    }
}
