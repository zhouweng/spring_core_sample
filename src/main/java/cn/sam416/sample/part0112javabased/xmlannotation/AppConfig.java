package cn.sam416.sample.part0112javabased.xmlannotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public
class AppConfig {
    @Bean
    public MyService myService() {
        return new MyServiceImpl();
    }
}