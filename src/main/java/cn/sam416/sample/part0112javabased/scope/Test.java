package cn.sam416.sample.part0112javabased.scope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        ServiceA serviceA1 = ctx.getBean(ServiceA.class);
        System.out.println(serviceA1);
        ServiceA serviceA2 = ctx.getBean(ServiceA.class);
        System.out.println(serviceA2);
    }
}
