package cn.sam416.sample.part0112javabased.scope;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {
    @Bean
    @Scope("prototype")
    public ServiceA serviceA() {
        return new ServiceA();
    }
}