package cn.sam416.sample.part0112javabased.lifecycle;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        ctx.registerShutdownHook(); // @1 trigger BeanTwo.cleanup()

    }
}
