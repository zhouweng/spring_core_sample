package cn.sam416.sample.part0112javabased.autowired;

public interface TransferService {
    void transfer(double amnt, String accountA, String accountB);
}
