package cn.sam416.sample.part0112javabased.loosecoupled;

public interface TransferService {
    void transfer(double amnt, String accountA, String accountB);
}
