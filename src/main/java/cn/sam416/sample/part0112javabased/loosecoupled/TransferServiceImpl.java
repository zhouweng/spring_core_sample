package cn.sam416.sample.part0112javabased.loosecoupled;

public class TransferServiceImpl implements TransferService {
    public TransferServiceImpl(AccountRepository accountRepository) {
    }


    @Override
    public void transfer(double amnt, String accountA, String accountB) {
        System.out.println("Transfer "+amnt);
    }
}
