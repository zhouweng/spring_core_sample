package cn.sam416.sample.part0112javabased.loosecoupled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@2 new DefaultRepositoryConfig class relate to ServiceConfig
public class DefaultRepositoryConfig implements RepositoryConfig {
    @Autowired
    private final DataSource dataSource;

    public DefaultRepositoryConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public AccountRepository accountRepository() {
        return new JdbcAccountRepository(dataSource);
    }
}
