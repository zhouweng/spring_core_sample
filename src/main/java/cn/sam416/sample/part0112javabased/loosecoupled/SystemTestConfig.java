package cn.sam416.sample.part0112javabased.loosecoupled;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ServiceConfig.class, DefaultRepositoryConfig.class}) //@ import the concrete config!
public class SystemTestConfig {
    @Bean
    public DataSource dataSource() {
        // return new DataSource
        return new DataSourceImpl() ;
    }
}
