package cn.sam416.sample.part0112javabased.loosecoupled;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@1 RepositoryConfig from class to interface
public interface RepositoryConfig {
    @Bean
    public AccountRepository accountRepository();
}
