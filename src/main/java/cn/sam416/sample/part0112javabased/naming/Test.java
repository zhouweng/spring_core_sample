package cn.sam416.sample.part0112javabased.naming;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
//@1 default        ServiceA serviceA1 = ctx.getBean(ServiceA.class);
//@2 use Naming
        ServiceA serviceA1 = ctx.getBean("bestService",ServiceA.class);
//@3 use Aliasing
//        ServiceA serviceA1 = ctx.getBean("service2",ServiceA.class);
        serviceA1.say();
    }
}
