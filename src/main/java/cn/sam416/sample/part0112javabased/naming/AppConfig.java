package cn.sam416.sample.part0112javabased.naming;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig {
    //    @Bean(name = "bestService" , initMethod = "init")
    @Bean({"service1", "service2", "service3"})
    public ServiceA serviceA() {
        return new ServiceA();
    }
}