package cn.sam416.sample.part0112javabased.naming;

public class ServiceA {
    void init() {
        System.out.println("ServiceA.init()");
    }

    void say() {
        System.out.println("serviceA.say()");
    }
}
