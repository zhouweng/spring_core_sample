package cn.sam416.sample.part0112javabased.importresource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:/cn/sam416/sample/part0112javabased/importresource-beans.xml")
public class ServiceConfig {
    @Autowired
    private ServiceA serviceA;

    @Bean
    public ServiceB serviceB() {
        return new ServiceB(serviceA);
    }
}
