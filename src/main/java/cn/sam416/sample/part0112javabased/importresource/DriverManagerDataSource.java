package cn.sam416.sample.part0112javabased.importresource;


public class DriverManagerDataSource implements DataSource {
    private String url;
    private String username;
    private String password;

    public DriverManagerDataSource(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public void say() {
        System.out.println(this.url);
    }
}
