package cn.sam416.sample.part0112javabased.importresource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctxService = new AnnotationConfigApplicationContext(ServiceConfig.class);
        ServiceA serviceA = ctxService.getBean(ServiceA.class);
        serviceA.say();

        ApplicationContext ctxApp = new AnnotationConfigApplicationContext(AppConfig.class);
        DataSource dataSource = ctxApp.getBean(DataSource.class);
        dataSource.say();
    }
}
