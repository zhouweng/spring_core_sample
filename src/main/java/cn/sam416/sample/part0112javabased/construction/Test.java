package cn.sam416.sample.part0112javabased.construction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

//@1 equivalent         ApplicationContext ctx = new AnnotationConfigApplicationContext(MyServiceImpl.class);

//@2 use register()
//        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//        ctx.register(AppConfig.class);
//        ctx.refresh();

//@3 use scan()
//        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//        ctx.scan("cn.sam416.sample.part0112javabased.construction");
//        ctx.refresh();

        MyService myService = ctx.getBean(MyService.class);
        myService.doStuff();
    }
}
