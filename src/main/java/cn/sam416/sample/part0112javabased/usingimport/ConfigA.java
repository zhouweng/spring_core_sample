package cn.sam416.sample.part0112javabased.usingimport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigA {
    @Bean
    public ServiceA serviceA() {
        return new ServiceA();
    }
}