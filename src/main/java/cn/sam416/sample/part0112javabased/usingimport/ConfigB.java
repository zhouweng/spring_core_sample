package cn.sam416.sample.part0112javabased.usingimport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ConfigA.class)
public class ConfigB {
    @Bean
    public ServiceB serviceB() {
        return new ServiceB();
    }
}