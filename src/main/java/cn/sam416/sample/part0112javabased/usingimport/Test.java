package cn.sam416.sample.part0112javabased.usingimport;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigB.class);

        // now both beans A and B will be available...
        ServiceA serviceA = ctx.getBean(ServiceA.class);
        ServiceB serviceB = ctx.getBean(ServiceB.class);
        serviceA.say();
        serviceB.say();

    }
}
